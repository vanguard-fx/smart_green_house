#include "PumpImpl.h"
#include <ServoTimer2.h> 

PumpImpl::PumpImpl(int pin) {
  this->pin = pin;
  myPump.attach(pin);
}

int PumpImpl::getAngle() {
  return this->myPump.read();
}

void PumpImpl::setAngle(int angle) {
  this->myPump.write(map(angle, 0, 181, 750, 2500));
}

void PumpImpl::closePump() {
  this->setAngle(0);
}

void PumpImpl::openPump() {
  this->setAngle(180);
}
