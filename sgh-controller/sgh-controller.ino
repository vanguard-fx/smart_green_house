/*
 * SEIOT a.a. 2018/2019
 *
 * CONSEGNA #3 - SMART GREEN HOUSE
 *
 * Donini Massimo, Hnatyshyn Nazar, Navacchia Mattia
 */

#include "Scheduler.h"
#include "SghAutoTask.h"
#include "SghManualTask.h"
#include "FillingTask.h"
#include "Environment.h"

/* Variables*/
Scheduler scheduler;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  scheduler.init(50);

  Environment* environment = new Environment();

  SghAutoTask* sghAuto = new SghAutoTask(environment);
  sghAuto->init(50);
  scheduler.addTask(sghAuto);
  SghManualTask* sghManual = new SghManualTask(environment);
  sghManual->init(50);
  scheduler.addTask(sghManual);
  FillingTask* sghFilling = new FillingTask(environment);
  sghFilling->init(50);
  scheduler.addTask(sghFilling);
}

void loop() {
  // put your main code here, to run repeatedly:
  scheduler.schedule();
}
