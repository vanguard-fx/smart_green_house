#include "SghAutoTask.h"
#include "Sonar.h"
#include "Led.h"
#include "config.h"
#include "MsgService.h"
#include "PumpImpl.h"

SghAutoTask::SghAutoTask(Environment* e) {
  this->environment = e;
  this->led = new Led(L1);
}

void SghAutoTask::init(int period) {
  Task::init(period);
  this->period = period;
  state = IDLE;
}

void SghAutoTask::tick() {
  waitTime += period;
  switch(state) {
    case IDLE: {
      if(environment->getSghMode() == AUTOMODE) {
        waitTime = 0;
        state = AUTO;
        led->switchOn();
      } else {
        led->switchOff();
        updateHumidity();
      }
      break;
    }
    case AUTO: {
      if(environment->getSghMode() == AUTOMODE) {
        updateHumidity();
        if (environment->getHumidity() < UMin && environment->getHumidity() != -1) {
          this->state = AUTOFILLING;
          environment->setPumpState(OPENED);
        }
      } else {
        this->state = IDLE;
      }
      break; 
    }
    case AUTOFILLING: {
      if (environment->getPumpState() == CLOSED) {
        state = IDLE;
      } else {
        updateHumidity();
      }
      break; 
    } 
  }
}

void SghAutoTask::updateHumidity() {
  if(waitTime >= REQUEST_RATE) {
    MsgService.sendMsg(MSG_REQUEST_HUM);
    waitTime = 0;
  }
  if (MsgService.isMsgAvailable()) {
    Msg* msg = MsgService.receiveMsg();
    this->environment->setHumidity(msg->getContent().toFloat());
    delete msg;
  }
}
