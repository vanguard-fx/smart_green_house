#include "Task.h"
#include "ProximitySensor.h"
#include "Led.h"
#include "Environment.h"
#include "config.h"
#include "MsgServiceBT.h"

class SghManualTask: public Task {

public:
  SghManualTask(Environment* e);
  void init(int period);
  void tick();

private:
  Environment* environment;
  ProximitySensor* pProx;
  Led* led;
  MsgServiceBT msgService;

  int period;
  int waitTime;
  
  void checkBT();
  void sendBT();

  enum { IDLE, WAITCONNECTION, MANUAL, MANUALFILLING } state;
};
