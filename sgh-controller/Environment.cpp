#include "Environment.h"
#include "Config.h"

Environment::Environment() {
  sghMode = AUTOMODE;
  pumpState = CLOSED;
  humidity = -1;
  pumpLoad = 0;
}

SghMode Environment::getSghMode() {
  return this->sghMode;
}

void Environment::setSghMode(SghMode sghMode) {
  this->sghMode = sghMode;
}

PumpState Environment::getPumpState() {
  return this->pumpState;
}

void Environment::setPumpState(PumpState pumpState) {
  this->pumpState = pumpState;
}

void Environment::setHumidity(int humidity) {
  this->humidity = humidity;
}

float Environment::getHumidity() {
  return this->humidity;
}

void Environment::setPumpLoad(int degree) {
  this->pumpLoad = degree;
}

int Environment::getPumpLoad() {
  return this->pumpLoad;
}
