#ifndef __ENVIRONMENT__
#define __ENVIRONMENT__

typedef enum {AUTOMODE, MANUALMODE} SghMode;
typedef enum {OPENED, CLOSED} PumpState;

class Environment {
public:
  Environment();

  //GETTERS
  SghMode getSghMode();
  PumpState getPumpState();
  float getHumidity();
  int getPumpLoad();

  //SETTERS
  void setSghMode(SghMode mode);
  void setPumpState(PumpState mode);
  void setHumidity(int humidity);
  void setPumpLoad(int degree);

private:
  SghMode sghMode;
  PumpState pumpState;
  float humidity;
  int pumpLoad;
};

#endif
