#ifndef __MSGSERVICEBT__
#define __MSGSERVICEBT__

#include "Arduino.h"
#include "SoftwareSerial.h"

class BTMsg {
  String content;

public:
  BTMsg(const String& content){
    this->content = content;
  }
  
  String getContent(){
    return content;
  }
};

class MsgServiceBT {
    
public: 
  MsgServiceBT();  
  void init(int rxPin, int txPin);  
  bool isMsgAvailable();
  BTMsg* receiveMsg();
  bool sendMsg(BTMsg msg);

private:
  String content;
  SoftwareSerial* channel;
  
};

#endif
