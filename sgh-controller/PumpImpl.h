#ifndef __PUMPIMPL__
#define __PUMPIMPL__

#include "Arduino.h"
#include "Pump.h"
#include <ServoTimer2.h> 

class PumpImpl: public Pump {

public: 
  PumpImpl(int pin);
  int getAngle();
  void setAngle(int angle);
  void closePump();
  void openPump();

private:
  ServoTimer2 myPump;
  int pin;
};

#endif
