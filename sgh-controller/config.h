#ifndef __CONFIG__
#define __CONFIG__

#define L1 13
#define L2 6
#define LM 12

#define SERVO 4

#define SONAR_T 8
#define SONAR_E 7

#define BT_T 2
#define BT_R 3

#define DIST 0.3

#define UMin 30.0
#define DeltaU 5.0
#define Tmax 5000

#define PMIN 45
#define PMID 90
#define PMAX 179

#define REQUEST_RATE 1000

#define MSG_REQUEST_HUM     "REQUEST_HUMIDITY"
#define MSG_PUMP_OPENED     "PUMP_OPENED"
#define MSG_PUMP_CLOSED     "PUMP_CLOSED"
#define MSG_TIMEOUT         "PUMP_TIMEOUT"
#define MSG_OPEN_MANUAL     "OPEN|"
#define MSG_CLOSE_MANUAL    "CLOSE"
#define MSG_CONNECTED       "CONNECTED"
#define MSG_DISCONNECTED    "DISCONNECTED"

#endif
