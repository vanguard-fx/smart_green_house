#ifndef __PUMP__
#define __PUMP__

class Pump {
 
public: 
  virtual int getAngle() = 0;
  virtual void setAngle(int angle) = 0;
  virtual void closePump() = 0;
  virtual void openPump() = 0;
};

#endif
