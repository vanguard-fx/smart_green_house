#include "FillingTask.h"
#include "Sonar.h"
#include "Led.h"
#include "LedExt.h"
#include "config.h"
#include "MsgService.h"
#include "PumpImpl.h"

FillingTask::FillingTask(Environment* e) {
  this->environment = e;
  this->led = new LedExt(L2);
  this->myPump = new PumpImpl(SERVO);
}

void FillingTask::init(int period) {
  Task::init(period);
  this->period = period;
  state = IDLE;
  manualSetPump(0);
}

void FillingTask::tick() {
  switch(state) {
    Serial.println(environment->getPumpLoad());
    case IDLE: {
      if(environment->getSghMode() == AUTOMODE && environment->getHumidity() < UMin && environment->getHumidity() != -1) {
        autoOpenPump(environment->getHumidity());
        state = FILLING;
      } else if(environment->getSghMode() == MANUALMODE && environment->getPumpLoad() != 0) {
        manualSetPump(environment->getPumpLoad());
        state = FILLING;
      }
      break;
    }
    case FILLING: {
      if(environment->getSghMode() == AUTOMODE) {
        autoFillingTime += period;
      } else if (oldPumpLoad != environment->getPumpLoad()){
        manualSetPump(environment->getPumpLoad(), false);
      }
      
      oldPumpLoad = environment->getPumpLoad();
      if((environment->getSghMode() == AUTOMODE && (environment->getHumidity() - DeltaU > UMin) || autoFillingTime > Tmax)|| 
          (environment->getSghMode() == MANUALMODE && environment->getPumpLoad() == 0)) {
          manualSetPump(0);
          if(autoFillingTime > Tmax) {
            MsgService.sendMsg(MSG_TIMEOUT);
          }
          autoFillingTime = 0;
          environment->setHumidity(-1);
          this->state = IDLE;
      }
      break; 
    }
  }
}

void FillingTask::autoOpenPump(float humidity) {
  if (humidity > 20.0 && humidity <= 30.0) {
      manualSetPump(PMIN);
    } else if (humidity > 10.0 && humidity <= 20.0) {
      manualSetPump(PMID);
    } else {
      manualSetPump(PMAX);
  }
}

void FillingTask::manualSetPump(int degree, bool sendMsg) {
  if(degree == 0) {
    environment->setPumpState(CLOSED);
    if (sendMsg) {
      MsgService.sendMsg(MSG_PUMP_CLOSED);
    }
  } else {
    environment->setPumpState(OPENED);
    if (sendMsg) {
      MsgService.sendMsg(MSG_PUMP_OPENED);
    }
  }
  led->setIntensity(map(degree, 0, 180, 0, 255));
  myPump->setAngle(degree);
}

void FillingTask::manualSetPump(int degree) {
  manualSetPump(degree, true);
}
