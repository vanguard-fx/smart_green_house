#include "SghManualTask.h"
#include "Sonar.h"
#include "Led.h"
#include "config.h"
#include "MsgService.h"
#include "MsgServiceBT.h"
#include "Environment.h"
#include <SoftwareSerial.h>

SghManualTask::SghManualTask(Environment* e) {
  this->environment = e;
  this->pProx = new Sonar(SONAR_E, SONAR_T);
  this->led = new Led(LM);
}

void SghManualTask::init(int period) {
  Task::init(period);
  this->period = period;
  state = IDLE;
}

void SghManualTask::tick() {
  switch(state) {
    case IDLE: 
      if( pProx->getDistance() < DIST ) {
        this->msgService.init(BT_T, BT_R);
        state = WAITCONNECTION;
      } else {
        led->switchOff();
        state = IDLE;
        environment->setSghMode(AUTOMODE);
      }
      break; 
    case WAITCONNECTION: {
      if( pProx->getDistance() < DIST ) {
        checkBT();
      } else {
        state = IDLE;
      }
      break; 
    }
    case MANUAL: {
      if( pProx->getDistance() < DIST ) {
        checkBT();
        sendBT();
      } else {
        state = IDLE;
      }
      break; 
    } 
    case MANUALFILLING: {
      if( pProx->getDistance() < DIST ) {
        checkBT();
        sendBT();
      } else {
        state = IDLE;
      }
      break; 
    } 
  }
}

void SghManualTask::checkBT() {
  String message = "";
  if (msgService.isMsgAvailable()) {
    BTMsg* msg = msgService.receiveMsg();
    if(msg->getContent().length() != 0) {
      message = msg->getContent();
    }
    delete msg;
  }
  if(message.indexOf(MSG_OPEN_MANUAL) == 0) {
    //Get a value from message in format "OPEN|000";
    int value = message.substring(sizeof(MSG_OPEN_MANUAL) - 1,message.length()).toInt();
    this->environment->setPumpLoad(value);
    state = MANUALFILLING;
  }
  if(message == MSG_CLOSE_MANUAL) {
    this->environment->setPumpLoad((int)0);
    state = MANUAL;
  }
  if(message == MSG_DISCONNECTED) {
    this->environment->setPumpLoad((int)0);
    environment->setSghMode(AUTOMODE);
    state = IDLE;
    led->switchOff();
  }
  if(message == MSG_CONNECTED) {
    environment->setSghMode(MANUALMODE);
    state = MANUAL;
    led->switchOn();
  }
}

void SghManualTask::sendBT() {
  waitTime += period;
  if (waitTime >= REQUEST_RATE) {
    msgService.sendMsg(BTMsg(String((int)environment->getHumidity())));
    waitTime = 0;
  }
}
