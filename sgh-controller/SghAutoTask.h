#include "Task.h"
#include "Led.h"
#include "Environment.h"
#include "config.h"
#include "Pump.h"

class SghAutoTask: public Task {

public:
  SghAutoTask(Environment* e);
  void init(int period);
  void tick();

private:
  Environment* environment;
  Led* led;

  int period;
  int waitTime;
  void updateHumidity();

  enum { IDLE, AUTO, AUTOFILLING } state;
};
