#include "Arduino.h"
#include "MsgServiceBT.h"


MsgServiceBT::MsgServiceBT(){
  
}

void MsgServiceBT::init(int rxPin, int txPin){
  channel = new SoftwareSerial(rxPin, txPin);
  content.reserve(256);
  channel->begin(9600);
}

bool MsgServiceBT::sendMsg(BTMsg msg){
  channel->println(msg.getContent());  
}

bool MsgServiceBT::isMsgAvailable(){
  return channel->available();
}

BTMsg* MsgServiceBT::receiveMsg(){
  BTMsg* msg = NULL;
  while (channel->available()) {
    char ch = (char) channel->read();
    if(ch == '\n') {
      msg = new BTMsg(content); 
      content = "";
    } else {
      content += ch;
    }
  }
  return msg;  
}
