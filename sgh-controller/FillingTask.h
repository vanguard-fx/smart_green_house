#include "Task.h"
#include "Led.h"
#include "Environment.h"
#include "config.h"
#include "Pump.h"

class FillingTask: public Task {

public:
  FillingTask(Environment* e);
  void init(int period);
  void tick();

private:
  Environment* environment;
  Led* led;
  Pump* myPump;
  int period;
  int autoFillingTime;
  int oldPumpLoad;

  void autoOpenPump(float humidity);
  void manualSetPump(int degree);
  void manualSetPump(int degree, bool sendMsg);

  enum { IDLE, FILLING} state;
};
