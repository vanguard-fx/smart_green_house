#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include "DHT.h"
#include "ArduinoJson.h"

#define DHTTYPE DHT11

#define POT_PIN A0

/* wifi network name */
const char* ssidName = "SSID";
/* WPA2 PSK password */
const char* pwd = "PWD";
/* service IP address */ 
char* address = "NGROK_LINK";

/* DHT sensor. */
const int DHTPin = 2;
/* Initialize DHT sensor. */
DHT dht(DHTPin, DHTTYPE);

//JSON RAM SIZE -> https://arduinojson.org/v6/assistant/
const size_t capacity = JSON_OBJECT_SIZE(3);

void setup() { 
  Serial.begin(115200);
  dht.begin();                           
  WiFi.begin(ssidName, pwd);
  Serial.print("Connecting...");
  while (WiFi.status() != WL_CONNECTED) {  
    delay(500);
    Serial.print(".");
  } 
  Serial.println("Connected");
}

int sendData(String address, String data){  
   HTTPClient http;    
   http.begin(address + "/api/data");      
   http.addHeader("Content-Type", "application/json");     
   int retCode = http.POST(data);   
   http.end();   
   return retCode;
}
   
void loop() { 
 if (WiFi.status()== WL_CONNECTED){   
    
    /* read sensor */
    float humidity = dht.readHumidity();
    float temperature = dht.readTemperature();

    if (isnan(humidity)) {
      humidity = -1;
    }
    if (isnan(temperature)) {
      temperature = -1;
    }

    //Creating JSON
    /*
    {
      "name": "sgh-edge",
      "humidity": 0,
      "temperature": 0
    }
    */
    
    DynamicJsonDocument doc(capacity);
    
    doc["name"] = "sgh-edge";
    doc["humidity"] = map(analogRead(POT_PIN), 0, 1023, 0, 50);
    //doc["humidity"] = humidity;
    doc["temperature"] = temperature;
    
    String json;
    serializeJson(doc, json);
    
    /* send data */
    Serial.print("sending " + json + "... ");
    int code = sendData(address, json);

    /* log result */
    if (code == 200){
     Serial.println("| [OK]");   
    } else {
     Serial.println("error: " +String(code));
    }
 } else { 
   Serial.println("Error in WiFi connection");   
 }
 
 delay(1000);  
}
