package com.example.iot.greenhousemobileapp;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class ControlActivity extends AppCompatActivity {

    Button btnOpenPump, btnClosePump, btnCloseConnection;
    TextView textHumidity, textLevelWater;
    SeekBar seekBar;
    String address = null;
    private ProgressDialog progress;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;

    // humidity
    byte buffer[];
    boolean stopThread;
    private InputStream inputStream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent newint = getIntent();
        address = newint.getStringExtra(MainActivity.EXTRA_ADDRESS); //receive the address of the bluetooth device

        setContentView(R.layout.activity_control);

        btnOpenPump = findViewById(R.id.buttonOpenPump);
        btnClosePump = findViewById(R.id.buttonClosePump);
        btnCloseConnection = findViewById(R.id.buttonCloseConnection);
        textHumidity = findViewById(R.id.textHumidity);
        textLevelWater = findViewById(R.id.textLevelWater);
        seekBar = findViewById(R.id.seekBar);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textLevelWater.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        new ConnectBT().execute();

        btnOpenPump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPump();
            }
        });

        btnClosePump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closePump();
            }
        });

        btnCloseConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnect();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        disconnect();
    }

    private void disconnect() {
        if (btSocket!=null) {
            //If the btSocket is busy
            try {
                sendDisconnection();
                isBtConnected = false;
                stopThread = true;
                inputStream.close();
                btSocket.close(); //close connection
            }
            catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error on disconnection()." ,Toast.LENGTH_LONG).show();
            }
        }
        finish(); //return to the first layout
    }

    private void closePump() {
        if (btSocket!=null) {
            try {
                this.sendMessage("CLOSE");
            }
            catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error on closePump()." ,Toast.LENGTH_LONG).show();
            }
        }
    }

    private void openPump() {
        if (btSocket!=null) {
            try {
                this.sendMessage("OPEN|" + textLevelWater.getText());
            }
            catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error on openPump()." ,Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendConnection() {
        if (btSocket!=null) {
            try {
                this.sendMessage("CONNECTED");
            }
            catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error on sendConnection()." ,Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendDisconnection() {
        if (btSocket!=null) {
            try {
                this.sendMessage("DISCONNECTED");
            }
            catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error on sendConnection()." ,Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendMessage(String msg) throws IOException{
        msg += '\n';
        btSocket.getOutputStream().write(msg.getBytes());
    }

    /*private void updateHumidity() throws IOException {
        int bytes = 0, offset = 0;
        byte[] buffer;
        bytes = btSocket.getInputStream().read();
        buffer = new byte[bytes];
        while (true)
        {
            bytes = btSocket.getInputStream().read(buffer, offset, buffer.length - offset);
            offset += bytes;
            if (bytes == -1 || offset >= buffer.length)
            {
                Log.d("MyBT", "message: " + new String(buffer));
                textHumidity.setText(new String(buffer));

                break;
            }
        }
    }*/

    private void listenForData() {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread  = new Thread(new Runnable() {
            public void run() {
                while(!Thread.currentThread().isInterrupted() && !stopThread) {
                    try {
                        int byteCount = btSocket.getInputStream().available();
                        if(byteCount > 0) {
                            byte[] rawBytes = new byte[byteCount];
                            inputStream.read(rawBytes);
                            final String string = new String(rawBytes,"UTF-8");
                            if (string.matches(".*\\d.*")) {
                                handler.post(new Runnable() {
                                    public void run() {
                                        textHumidity.setText("LEVEL OF HUMIDITY: " + string.trim());
                                    }
                                });
                            }
                        }
                    }
                    catch (IOException ex) {
                        stopThread = true;
                    }
                }
            }
        });

        thread.start();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void> {
        // UI thread
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(ControlActivity.this, "Connecting...", "Please wait!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) {
            //while the progress dialog is shown, the connection is done in background
            try {
                if (btSocket == null || !isBtConnected) {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e) {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            //after the doInBackground, it checks if everything went fine
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                Toast.makeText(getApplicationContext(), "Connection Failed. Is it a SPP Bluetooth? Try again." ,Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                Toast.makeText(getApplicationContext(), "Connected." ,Toast.LENGTH_LONG).show();
                isBtConnected = true;
                sendConnection();
            }
            progress.dismiss();
            try {
                inputStream = btSocket.getInputStream();
                listenForData();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
