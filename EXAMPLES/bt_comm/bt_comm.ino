#include "MsgServiceBT.h"
#include <SoftwareSerial.h>

#define MSG_OPEN_MANUAL     "OPEN|"
#define MSG_CLOSE_MANUAL    "CLOSE"

MsgServiceBT msgService(2,3);

void setup() {
  Serial.begin(9600);
  Serial.println("READY!");
  
  msgService.init();
}

void loop() {
  if (msgService.isMsgAvailable()) {
    Msg* msg = msgService.receiveMsg();
    if(msg->getContent().length() != 0) {
      String message = msg->getContent();
      if(message.indexOf(MSG_OPEN_MANUAL) == 0) {
        //Get a value from message in format "OPEN|000";
        int value = message.substring(sizeof(MSG_OPEN_MANUAL) - 1,message.length()).toInt();
        Serial.print("Open Pump on: ");
        Serial.println(value);
      }
      if(message == MSG_CLOSE_MANUAL) {
        Serial.println("Close Pump");
      }
    }
    delete msg;
  }
  msgService.sendMsg(Msg("10"));
  delay(50);
}
