package sgh.serial.data;

public class PumpData {

    public enum PumpState { CLOSED, OPENED }
    private PumpState pumpState;
    private String time;
        
    public PumpData(final PumpState pumpState, final String time) {
        this.pumpState = pumpState;
        this.time = time;
    }
    
    public PumpState getPumpState() {
        return pumpState;
    }

    public String getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "PumpData [pumpState=" + pumpState + ", time=" + time + "]";
    }
    
}