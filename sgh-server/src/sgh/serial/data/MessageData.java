package sgh.serial.data;

public class MessageData {
    private String message;
    private String time;
    
    public MessageData(final String message, final String time) {
        this.message = message;
        this.time = time;
    }
    
    public String getMessage() {
        return message;
    }
    
    public String getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "MessageData [message=" + message + ", time=" + time + "]";
    }

}
