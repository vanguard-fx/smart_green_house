package sgh.serial;

import java.sql.Timestamp;
import java.util.LinkedList;

import sgh.serial.common.*;
import sgh.serial.devices.*;
import sgh.server.DataPoint;
import sgh.serial.common.Event;
import sgh.serial.data.MessageData;
import sgh.serial.data.PumpData;
import sgh.serial.data.PumpData.PumpState;

public class MyAgent extends BasicEventLoopController {
    private static final int MAX_SIZE = 50;
    
    private MsgService msgService;
    private ObservableTimer timer;
    
    private LinkedList<DataPoint> values;
    private LinkedList<PumpData> pumpStates;
    private LinkedList<MessageData> messages;

    private enum State {
        WAIT_FOR_REQUEST, WAIT_FOR_TICK
    };

    private State currentState;

    public MyAgent(String port, int rate, LinkedList<DataPoint> values, LinkedList<PumpData> pumpStates, LinkedList<MessageData>messages) {
        this.msgService = new MsgService(port, rate);
        this.values = values;
        this.pumpStates = pumpStates;
        this.messages = messages;
        msgService.init();
        this.timer = new ObservableTimer();
        timer.addObserver(this);
        msgService.addObserver(this);
        currentState = State.WAIT_FOR_REQUEST;
    }

    protected void processEvent(Event ev) {
        switch (currentState) {
        case WAIT_FOR_REQUEST:
            try {
                if (ev instanceof MsgEvent) {
                    String msg = ((MsgEvent) ev).getMsg();
                    //System.out.println("Received: " + msg);
                    switch (msg) {
                    case "PUMP_OPENED":
                        addPumpData(new PumpData(PumpState.OPENED, new Timestamp(System.currentTimeMillis()).toString()));
                        break;
                    case "PUMP_CLOSED":
                        addPumpData(new PumpData(PumpState.CLOSED, new Timestamp(System.currentTimeMillis()).toString()));
                        break;
                    case "PUMP_TIMEOUT":
                        messages.addFirst(new MessageData("TimeOut", new Timestamp(System.currentTimeMillis()).toString()));
                        if (messages.size() > MAX_SIZE) {
                            messages.removeLast();
                        }
                        break;
                    default:
                        timer.start(500);
                        currentState = State.WAIT_FOR_TICK;
                        break;
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            break;
        case WAIT_FOR_TICK:
            if (ev instanceof Tick) {
                timer.stop();
                String msg = "-1";
                if (!values.isEmpty()) {
                    msg = values.getFirst().getHumidity().toString();
                }
                //System.out.println("Sending: " + msg);
                msgService.sendMsg(msg);
                currentState = State.WAIT_FOR_REQUEST;
            }
            break;
        }
    }
    
    private void addPumpData(PumpData pumpData) {
        pumpStates.addFirst(pumpData);
        if (pumpStates.size() > MAX_SIZE) {
            pumpStates.removeLast();
        }
    }
}
