package sgh.client;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonObject;

public class TestClient extends AbstractVerticle {
	
	public static void main(String[] args) {		
	
		String host = "localhost";
		int port = 8080;

		Vertx vertx = Vertx.vertx();
		HttpClient client = vertx.createHttpClient();
		
                /* {
                      "name": "sgh-edge",
                      "humidity": 0,
                      "temperature": 0
                    }
                 */
		JsonObject item = new JsonObject();
		item.put("name", "sgh-edge");
		item.put("humidity", 31);
		item.put("temperature", 30);
		
		//POST REQUEST
		client.post(port, host, "/api/data", response -> {
			System.out.println("Received response with status code " + response.statusCode());
			response.bodyHandler(bodyHandler -> {
				System.out.println(bodyHandler.toString());
			});
		}).putHeader("content-type", "application/json").end(item.encodePrettily());
		
		
		//GET REQUEST
        		//values
        		//pumpStates
        		//messages
		 client.get(port, host, "/api/data?type=messages", response -> {
                     System.out.println("Received response with status code " + response.statusCode());
                     response.bodyHandler(bodyHandler -> {
                             System.out.println(bodyHandler.toString());
                     });
		 }).putHeader("content-type", "application/json").end();
	}
	
}
