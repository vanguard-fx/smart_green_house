package sgh.server;

public class DataPoint {

    private String name;
    private String time;
    private Float humidity;
    private Float temperature;
    
    
    public DataPoint(final String name, final String time, final Float humidity, final Float temperature) {
        this.name = name;
        this.time = time;
        this.humidity = humidity;
        this.temperature = temperature;
    }
    
    public String getName() {
        return name;
    }
    public String getTime() {
        return time;
    }
    public Float getHumidity() {
        return humidity;
    }
    public Float getTemperature() {
        return temperature;
    }
    @Override
    public String toString() {
        return "DataPoint [name=" + name + ", time=" + time + ", humidity=" + humidity + ", temperature="
                + temperature + "]";
    }
}