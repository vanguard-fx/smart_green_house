package sgh.server;

import java.util.LinkedList;

import io.vertx.core.Vertx;
import sgh.serial.MyAgent;
import sgh.serial.data.MessageData;
import sgh.serial.data.PumpData;

public class Main {
    
    private static final int PORT = 8080;
    private static final int RATE = 9600;
    
    private static void startServices() {
        LinkedList<DataPoint> values = new LinkedList<>();
        LinkedList<PumpData> pumpStates = new LinkedList<>();
        LinkedList<MessageData> messages = new LinkedList<>();
        
        Vertx vertx = Vertx.vertx();
        DataService service = new DataService(PORT, values, pumpStates, messages);
        vertx.deployVerticle(service);
    
        String portName = "COM4"; /* replace with the name of the serial port */
        MyAgent agent = new MyAgent(portName, RATE, values, pumpStates, messages);
        agent.start();
    }
    
    public static void main(String[] args) {
        startServices();
    }
}