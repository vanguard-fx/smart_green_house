package sgh.server;

import java.sql.Timestamp;
import java.util.LinkedList;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import sgh.serial.data.MessageData;
import sgh.serial.data.PumpData;

/*
 * Data Service as a vertx event-loop 
 */
public class DataService extends AbstractVerticle {

    private int port;
    private static final int MAX_SIZE = 50;
    private LinkedList<DataPoint> values;
    private LinkedList<PumpData> pumpStates;
    private LinkedList<MessageData> messages;

    public DataService(int port, LinkedList<DataPoint> values, LinkedList<PumpData> pumpStates, LinkedList<MessageData> messages) {
        this.values = values;
        this.port = port;
        this.pumpStates = pumpStates;
        this.messages = messages;
    }

    @Override
    public void start() {
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.post("/api/data").handler(this::handleAddNewData);
        router.get("/api/data").handler(this::handleGetData);
        vertx.createHttpServer().requestHandler(router::accept).listen(port);

        log("Service ready.");
    }

    private void handleAddNewData(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        // log("new msg "+routingContext.getBodyAsString());
        JsonObject res = routingContext.getBodyAsJson();
        if (res == null) {
            sendError(400, response);
        } else {
            String name = res.getString("name");
            String time = new Timestamp(System.currentTimeMillis()).toString();
            Float humidity = res.getFloat("humidity");
            Float temperature = res.getFloat("temperature");
            
            DataPoint dataPoint = new DataPoint(name, time, humidity, temperature);

            values.addFirst(dataPoint);
            if (values.size() > MAX_SIZE) {
                values.removeLast();
            }
            //log("New value: " + dataPoint.toString());
            response.setStatusCode(200).end();
        }
    }

    private void handleGetData(RoutingContext routingContext) {
        JsonArray arr = new JsonArray();
        String type = routingContext.request().getParam("type");
        switch (type) {
        case "values":
            for (DataPoint p : values) {
                JsonObject data = new JsonObject();
                data.put("sensor", p.getName());
                data.put("time", p.getTime());
                data.put("humidity", p.getHumidity());
                data.put("temperature", p.getTemperature());
                arr.add(data);
            }
            break;
        case "pumpStates":
            for (PumpData p : pumpStates) {
                JsonObject data = new JsonObject();
                data.put("pumpState", p.getPumpState());
                data.put("time", p.getTime());
                arr.add(data);
            }
            break;
        case "messages":
            for (MessageData p : messages) {
                JsonObject data = new JsonObject();
                data.put("message", p.getMessage());
                data.put("time", p.getTime());
                arr.add(data);
            }
            
            break;
        default:
            
            break;
        }
        if(arr.isEmpty()) {
            arr.add(new JsonObject().put("data", "null"));
        }
        if (type.isEmpty()) {
            routingContext.response().setStatusCode(404).end();
        } else {
            routingContext.response().putHeader("content-type", "application/json").end(arr.encodePrettily());
        }
    }

    private void sendError(int statusCode, HttpServerResponse response) {
        response.setStatusCode(statusCode).end();
    }

    private void log(String msg) {
        System.out.println("[DATA SERVICE] " + msg);
    }
}