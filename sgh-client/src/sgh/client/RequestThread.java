package sgh.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import javafx.application.Platform;
import javafx.util.Pair;

public class RequestThread extends Thread {
    // NGROK
    // private static final String HOST = "32d29257.ngrok.io";
    // private static final int PORT = 80;
    // LOCALHOST
    private static final String HOST = "localhost";
    private static final int PORT = 8080;
    private ViewController view;
    private boolean running;
    HttpClient client;
    Vertx vertx;
    private float humidity;
    private String pumpState;
    List<Pair<String, Integer>> historyHumidity;
    List<Pair<String, String>> historyPump;
    List<String> historyReports;

    public RequestThread(ViewController view) throws Exception {
        this.view = view;
        vertx = Vertx.vertx();
        client = vertx.createHttpClient();
        humidity = -1;
        pumpState = "CLOSE";
        historyHumidity = new ArrayList<>();
        historyPump = new ArrayList<>();
        historyReports = new ArrayList<>();
    }

    public void run() {
        this.running = true;
        while (this.running) {
            try {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        // HUMIDITY
                        client.get(PORT, HOST, "/api/data?type=values", response -> {
                            response.bodyHandler(bodyHandler -> {
                                // Last value
                                JsonArray jsonArray = bodyHandler.toJsonArray();
                                JsonObject jsonObject = jsonArray.getJsonObject(0);
                                if (jsonObject != null) {
                                    humidity = jsonObject.getFloat("humidity");
                                }
                                historyHumidity.clear();
                                // History
                                for (int i = jsonArray.size() - 1; i > 0; i--) {
                                    JsonObject j = jsonArray.getJsonObject(i);
                                    historyHumidity.add(new Pair<>(j.getString("time"), j.getInteger("humidity")));
                                }
                            });
                        }).putHeader("content-type", "application/json").end();

                        // PUMP STATE
                        client.get(PORT, HOST, "/api/data?type=pumpStates", response -> {
                            response.bodyHandler(bodyHandler -> {
                                // Last value
                                JsonArray jsonArray = bodyHandler.toJsonArray();
                                JsonObject jsonObject = jsonArray.getJsonObject(0);
                                if (jsonObject != null) {
                                    pumpState = jsonObject.getString("pumpState");
                                }
                                
                                historyPump.clear();
                                // History
                                for (int i = jsonArray.size() - 1; i > 0; i--) {
                                    JsonObject j = jsonArray.getJsonObject(i);
                                    historyPump.add(new Pair<>(j.getString("time"), j.getString("pumpState")));
                                }
                            });
                        }).putHeader("content-type", "application/json").end();

                        // REPORTS
                        client.get(PORT, HOST, "/api/data?type=messages", response -> {
                            response.bodyHandler(bodyHandler -> {
                                historyReports.clear();
                                // History
                                JsonArray jsonArray = bodyHandler.toJsonArray();
                                for (int i = jsonArray.size() - 1; i > 0; i--) {
                                    JsonObject j = jsonArray.getJsonObject(i);
                                    historyReports.add(j.getString("time") + "\t " + j.getString("message"));
                                }
                            });
                        }).putHeader("content-type", "application/json").end();

                        //Call view controller update methonds
                        view.updateHum(humidity);
                        view.updatePumpState(pumpState);
                        view.updateGraphHumidity(Collections.unmodifiableList(historyHumidity));
                        view.updateGraphPump(Collections.unmodifiableList(historyPump));
                        view.updateReports(Collections.unmodifiableList(historyReports));
                    }
                });
                Thread.sleep(1000);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void stopRunning() {
        this.running = false;
    }
}
