package sgh.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Client.fxml"));
        ViewController viewController = new ViewController();
        loader.setController(viewController);
        
        Platform.runLater(() -> {
            try {
                primaryStage.setTitle("Smart Green House");
                primaryStage.setScene(new Scene(loader.load()));
                primaryStage.show();
                primaryStage.setOnCloseRequest(e -> System.exit(0));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        
    }

    public static void main(String[] args) {
        launch(args);
    }
}
