package sgh.client;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;

public class ViewController {
    private Controller controller;

    XYChart.Series<String, Number> seriesHumidity;
    XYChart.Series<String, Number> seriesPumpStates;

    final ObservableList<String> reports = FXCollections.observableArrayList();

    @FXML
    Label labelHumidity, labelPumpState;

    @FXML
    LineChart<String, Number> graphHumidity;

    @FXML
    LineChart<String, Number> graphPumpStates;

    public ViewController() {
        this.controller = new Controller();
        this.controller.addUI(this);
        seriesHumidity = new XYChart.Series<>();
        seriesHumidity.setName("Humidity");
        seriesPumpStates = new XYChart.Series<>();
        seriesPumpStates.setName("PumpStates");
    }

    @SuppressWarnings("unchecked")
    @FXML
    private void initialize() {
        System.out.println("Initialize");
        try {
            this.controller.connect();
            graphHumidity.getData().addAll(seriesHumidity);
            graphPumpStates.getData().addAll(seriesPumpStates);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateHum(float humidity) {
        this.labelHumidity.setText("LEVEL OF HIMIDITY: " + humidity);
    }

    public void updatePumpState(String state) {
        this.labelPumpState.setText("PUMP'S STATE: " + state);
    }

    public void updateGraphHumidity(final List<Pair<String, Integer>> history) {
        try {
            if (!history.isEmpty()) {
                // The List is populating in another thread, wait for result
                Thread.sleep(100);
                // populating the series with data
                seriesHumidity.getData().clear();
                history.forEach(h -> {
                    try {
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(h.getKey());
                        String newDate = new SimpleDateFormat("HH:mm:ss").format(date);
                        seriesHumidity.getData().add(new XYChart.Data<String, Number>(newDate, h.getValue()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    public void updateGraphPump(final List<Pair<String, String>> history) {
        try {
            if (!history.isEmpty()) {
                // The List is populating in another thread, wait for result
                Thread.sleep(100);
                // populating the series with data
                seriesPumpStates.getData().clear();
                history.forEach(h -> {
                    try {
                        Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(h.getKey());
                        String newDate = new SimpleDateFormat("HH:mm:ss").format(date);
                        //Double data to make a graph in square wave( _-_-_-_ ) format and not triangular( /\/\/\/\ )
                        seriesPumpStates.getData().add(new XYChart.Data<String, Number>(newDate, (h.getValue().equals("OPENED") ? 1 : 0)));
                        seriesPumpStates.getData().add(new XYChart.Data<String, Number>(newDate, (h.getValue().equals("OPENED") ? 0 : 1)));
                        //System.out.println(newDate + " | " + (h.getValue().equals("OPENED") ? 1 : 0));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }

    public void updateReports(final List<String> reports) {
        this.reports.clear();
        this.reports.addAll(reports);
    }

    @FXML
    public void openReports() {
        final Stage dialog = new Stage();
        VBox dialogVbox = new VBox();
        dialogVbox.setAlignment(Pos.TOP_CENTER);

        final ListView<String> listView = new ListView<>();
        listView.setPrefSize(400, 350);
        listView.setItems(this.reports);

        dialogVbox.getChildren().add(listView);

        Scene dialogScene = new Scene(dialogVbox, 600, 350);
        dialog.setScene(dialogScene);
        dialog.show();
    }
}
