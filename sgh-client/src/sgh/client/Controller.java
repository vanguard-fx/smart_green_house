package sgh.client;

public class Controller {
    
    private ViewController viewController;

    public Controller() {

    }

    public void addUI(final ViewController viewController) {
        this.viewController = viewController;
    }

    public void connect() throws Exception {
        RequestThread requestThread = new RequestThread(this.viewController);
        requestThread.start();
    }
}
